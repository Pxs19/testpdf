import { Injectable } from '@nestjs/common';
import { PDFDocument, PDFFont, rgb } from 'pdf-lib';
import fontkit from '@pdf-lib/fontkit';
import * as fs from 'fs';
import { StandardFonts } from 'pdf-lib'
import { from } from 'rxjs';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async createPDF(): Promise<any> {
    try {
      const pdfDoc = await PDFDocument.load(
        fs.readFileSync('./src/file/filled4.pdf'),
      );
      let fontBytes = fs.readFileSync(
        './src/file/NotoSansThai-Regular.ttf',
      );

      const fieldNames = pdfDoc
        .getForm()
        .getFields()
        .map((f) => f.getName());

      console.log(fontBytes);
      pdfDoc.registerFontkit(fontkit);
      // const ubuntuFont = await pdfDoc.embedFont(StandardFonts.TimesRomanItalic);  //StandardFonts.TimesRomanItalic
      const ubuntuFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

      
      console.log(ubuntuFont)
     

      const fontSize = 12;
      const form = pdfDoc.getForm();
      
      // const filteredText = this.filterCharSet('กกหห', ubuntuFont);
      form.getTextField(fieldNames[0]).setText('');
      form.getTextField(fieldNames[1]).setText('25-08-2023');
      form.getTextField(fieldNames[2]).setText('Laurenn Isen');
      form.getTextField(fieldNames[3]).setText('');
      form.getTextField(fieldNames[4]).setText('02/02/1990');
      form.getTextField(fieldNames[5]).setText('25');
      form.getTextField(fieldNames[6]).setText('AB');
      form.getTextField(fieldNames[7]).setText('Hatyai Songkla 90110');
      form.getTextField(fieldNames[8]).setText('');
      form.getTextField(fieldNames[9]).setText('90110');
      form.getTextField(fieldNames[10]).setFontSize(10)
      form.getTextField(fieldNames[10]).setText('094672283443');
      form.getTextField(fieldNames[11]).setFontSize(10)
      form.getTextField(fieldNames[11]).setText('Buddhism');
      form.getTextField(fieldNames[12]).setFontSize(10)
      form.getTextField(fieldNames[12]).setText('student');
      form.getTextField(fieldNames[13]).setFontSize(10)
      form.getTextField(fieldNames[13]).setText('65');
      form.getTextField(fieldNames[14]).setFontSize(10)
      form.getTextField(fieldNames[14]).setText('185');
      form.getTextField(fieldNames[15]).setFontSize(10)
      form.getTextField(fieldNames[15]).setText('lauren@len.com');


      const pdfBytes = await pdfDoc.save();
      return pdfBytes;

    } catch (error) {
      console.log(error);
      throw new Error(`Failed to load PDF file: ${error.message}`);
    }
  }



}
